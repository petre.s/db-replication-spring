#!/bin/bash

# Init the database by creating a backup from master
echo "Init the database from backup"
su postgres -c "pg_basebackup -h $POSTGRES_MASTER_HOST -D $PGDATA -P -U $POSTGRES_USER --wal-method=stream"

# Create recovery.conf file in $PGDATA dir
RECOVERY_CONF="
    standby_mode = 'on'
    primary_conninfo = 'host=$POSTGRES_MASTER_HOST port=$POSTGRES_MASTER_PORT user=$POSTGRES_USER password=$POSTGRES_PASSWORD'
    trigger_file = '/path_to/trigger'
    restore_command = 'cp $PGDATA/archive/%f \"%p\"'
"
echo "writing recovery.conf"
su postgres -c "echo \"$RECOVERY_CONF\" > $PGDATA/recovery.conf"
