manual steps :
pg-a:

su - postgres && mkdir -p /var/lib/postgresql/data/archive && chmod 700 /var/lib/postgresql/data/archive && chown -R postgres:postgres /var/lib/postgresql/data/archive

pg_basebackup -h postgres-a -D $PGDATA/slave_data -P -U test1 --wal-method=stream

pg-b:
su - postgres
mkdir slave_data
chmod 700 slave_data/

pg_basebackup -h postgres-a -D $PGDATA/slave_data -P -U test1 --wal-method=stream