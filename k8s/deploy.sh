#!/bin/bash

kubectl create -f ./persistent-volumes/postgres-a-pvc.yaml
kubectl create -f ./deployments/postgres-a-deployment.yaml
kubectl create -f ./deployments/postgres-b-deployment.yaml
# kubectl create -f ./services/postgres-a-service.yaml
# kubectl create -f ./services/postgres-b-service.yaml
