CREATE TABLE books (
  id serial,
  name varchar(100) NOT NULL,
  author varchar(50) NOT NULL,
  PRIMARY KEY (id)
)
