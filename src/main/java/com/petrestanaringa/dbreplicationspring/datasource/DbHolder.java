package com.petrestanaringa.dbreplicationspring.datasource;

import javax.sql.DataSource;

public class DbHolder {

    private DataSource dataSource;

    public DbHolder(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public DataSource getDataSource() {
        return dataSource;
    }
}
