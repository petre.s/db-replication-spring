package com.petrestanaringa.dbreplicationspring.datasource;

public class DbWriteSync implements AutoCloseable {

    public DbWriteSync() {
        DbWriteManager.acquire();
    }

    @Override
    public void close() {
        DbWriteManager.release();
    }
}
