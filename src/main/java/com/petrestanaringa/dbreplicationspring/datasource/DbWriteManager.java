package com.petrestanaringa.dbreplicationspring.datasource;

import java.util.function.Supplier;

public class DbWriteManager {

    private static ThreadLocal<Boolean> locks = new ThreadLocal<>();
    private static Supplier<Boolean> transactionDetector = DbWriteManager::transactionDetectorDefault;

    static Boolean transactionDetectorDefault() {
        throw new IllegalStateException("transactionDetector needs to be set");
    }

    static void release() {
        locks.remove();
    }

    static void acquire() {
        if (transactionDetector.get() && !isAcquired()) {
            throw new IllegalStateException("acquire must be called *before* starting a transaction");
        }
        locks.set(Boolean.TRUE);
    }

    static boolean isAcquired() {
        return locks.get() != null;
    }

    public static void setTransactionDetector(Supplier<Boolean> transactionDetector) {
        DbWriteManager.transactionDetector = transactionDetector;
    }
}
