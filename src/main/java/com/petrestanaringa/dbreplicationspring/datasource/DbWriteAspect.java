package com.petrestanaringa.dbreplicationspring.datasource;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class DbWriteAspect {

    @Before("@annotation(com.petrestanaringa.dbreplicationspring.datasource.DbWrite)")
    private void beforeDbWrite() {
        DbWriteManager.acquire();
    }

    @After("@annotation(com.petrestanaringa.dbreplicationspring.datasource.DbWrite)")
    private void afterDbWrite() {
        DbWriteManager.release();
    }
}
