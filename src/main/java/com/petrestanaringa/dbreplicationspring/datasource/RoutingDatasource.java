package com.petrestanaringa.dbreplicationspring.datasource;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.jdbc.datasource.AbstractDataSource;


public class RoutingDatasource extends AbstractDataSource {

    private DataSource master;
    private DataSource standby;
    private boolean delegateToStandBy = false;
    public RoutingDatasource(DataSource master, DataSource standby) {
        this.master = master;
        this.standby = standby;
    }

    public void startDelegateToStandby() {
        delegateToStandBy = true;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }

    private DataSource getDataSource() {
        return !delegateToStandBy || DbWriteManager.isAcquired() ? master : standby;
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return getDataSource().getConnection(username, password);
    }
}
