package com.petrestanaringa.dbreplicationspring.datasource;

import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;

public class RoutingTransactionManager extends JpaTransactionManager {

    @Override
    protected void doBegin(Object transaction, TransactionDefinition definition) {
        DbWriteManager.acquire();
        try {
            super.doBegin(transaction, definition);
        } catch (RuntimeException e) {
            DbWriteManager.release();
            throw e;
        }
    }

    @Override
    protected void doCleanupAfterCompletion(Object transaction) {
        try {
            super.doCleanupAfterCompletion(transaction);
        } finally {
            DbWriteManager.release();
        }
    }
}
