package com.petrestanaringa.dbreplicationspring.datasource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("db")
public class DataSourceList {

    private DataSourceProperties master;
    private DataSourceProperties standby;

    public DataSourceProperties getMaster() {
        return master;
    }

    public void setMaster(DataSourceProperties master) {
        this.master = master;
    }

    public DataSourceProperties getStandby() {
        return standby;
    }

    public void setStandby(DataSourceProperties standby) {
        this.standby = standby;
    }
}
