package com.petrestanaringa.dbreplicationspring.repositories;

import com.petrestanaringa.dbreplicationspring.entities.Book;
import org.springframework.data.repository.CrudRepository;

public interface BooksRepository extends CrudRepository<Book, Integer> {

}
