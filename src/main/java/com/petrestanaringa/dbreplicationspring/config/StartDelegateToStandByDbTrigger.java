package com.petrestanaringa.dbreplicationspring.config;

import com.petrestanaringa.dbreplicationspring.datasource.RoutingDatasource;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class StartDelegateToStandByDbTrigger implements ApplicationListener<ContextRefreshedEvent> {

    private RoutingDatasource routingDatasource;

    public StartDelegateToStandByDbTrigger(RoutingDatasource routingDatasource) {
        this.routingDatasource = routingDatasource;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        routingDatasource.startDelegateToStandby();
    }
}
