package com.petrestanaringa.dbreplicationspring.config;

import com.petrestanaringa.dbreplicationspring.datasource.DataSourceList;
import com.petrestanaringa.dbreplicationspring.datasource.DataSourceProperties;
import com.petrestanaringa.dbreplicationspring.datasource.DbHolder;
import com.petrestanaringa.dbreplicationspring.datasource.DbWriteManager;
import com.petrestanaringa.dbreplicationspring.datasource.RoutingDatasource;
import com.petrestanaringa.dbreplicationspring.datasource.RoutingTransactionManager;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionSynchronizationManager;

@Configuration
public class Config {

    public static final String WRITE_TRANSACTION_MANAGER = "masterTransactionManager";

    @Autowired
    private DataSourceList dataSourceList;

    // using DbHolder for individual datasources, to avoid spring confusion
    // https://github.com/spring-projects/spring-boot/issues/9394
    @Bean
    public DataSource dataSource(
        @Qualifier("masterDatasource") DbHolder master,
        @Qualifier("standbyDatasource") DbHolder standby) {
        DataSource routingDataSource = new RoutingDatasource(master.getDataSource(), standby.getDataSource());
        DbWriteManager.setTransactionDetector(TransactionSynchronizationManager::isActualTransactionActive);
        return routingDataSource;
    }

    @Bean
    public DbHolder masterDatasource() {
        return buildDataSource(dataSourceList.getMaster());
    }

    @Bean
    public DbHolder standbyDatasource() {
        return buildDataSource(dataSourceList.getStandby());
    }

    @Bean(WRITE_TRANSACTION_MANAGER)
    public PlatformTransactionManager masterTransactionManager() {
        return new RoutingTransactionManager();
    }

    // keep the default bean name for compatibility
    @Bean("transactionManager")
    public PlatformTransactionManager standbyTransactionManager() {
        return new JpaTransactionManager();
    }

    private DbHolder buildDataSource(DataSourceProperties dataSourceProperties) {
        return new DbHolder(DataSourceBuilder
            .create()
            .username(dataSourceProperties.getUsername())
            .password(dataSourceProperties.getPassword())
            .url(dataSourceProperties.getUrl())
            .driverClassName(dataSourceProperties.getDriverClassName())
            .build());
    }
}
