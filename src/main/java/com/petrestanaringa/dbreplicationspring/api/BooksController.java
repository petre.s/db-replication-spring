package com.petrestanaringa.dbreplicationspring.api;

import com.petrestanaringa.dbreplicationspring.entities.Book;
import com.petrestanaringa.dbreplicationspring.services.BooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = "/books")
public class BooksController {

    @Autowired
    private BooksService booksService;

    @GetMapping
    public Iterable<Book> getAll() {
        return booksService.getAll();
    }

    @PostMapping
    public Book newBook(Book book) {
        return booksService.addNewBook(book);
    }
}
