package com.petrestanaringa.dbreplicationspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class DbReplicationSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbReplicationSpringApplication.class, args);
    }
}

