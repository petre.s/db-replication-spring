package com.petrestanaringa.dbreplicationspring.services;


import com.petrestanaringa.dbreplicationspring.entities.Book;

public interface BooksService {

    Iterable<Book> getAll();

    Book addNewBook(Book book);
}
