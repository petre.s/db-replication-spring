package com.petrestanaringa.dbreplicationspring.services;

import com.petrestanaringa.dbreplicationspring.config.Config;
import com.petrestanaringa.dbreplicationspring.entities.Book;
import com.petrestanaringa.dbreplicationspring.exceptions.ValidationException;
import com.petrestanaringa.dbreplicationspring.repositories.BooksRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookServiceImpl implements BooksService {

    private BooksRepository booksRepository;

    @Autowired
    public BookServiceImpl(BooksRepository booksRepository) {
        this.booksRepository = booksRepository;
    }

    @Override
    public Iterable<Book> getAll() {
        return booksRepository.findAll();
    }


    @Transactional(Config.WRITE_TRANSACTION_MANAGER)
    @Override
    public Book addNewBook(Book book) {
        if (Strings.isBlank(book.getName()) || Strings.isBlank(book.getAuthor())){
            throw new ValidationException("name or author cannot be blank");
        }

        book.setId(null);
        return booksRepository.save(book);
    }

}
