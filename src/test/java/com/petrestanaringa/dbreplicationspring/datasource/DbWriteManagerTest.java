package com.petrestanaringa.dbreplicationspring.datasource;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DbWriteManagerTest {

    @Before
    public void setUp() {
        DbWriteManager.release();
        DbWriteManager.setTransactionDetector(DbWriteManager::transactionDetectorDefault);
    }

    @Test(expected = IllegalStateException.class)
    public void acquireWithoutSettingTheDetector() {
        // given
        // no initialisation for transaction detector

        // when
        DbWriteManager.acquire();

        // then exception is thrown
    }

    @Test(expected = IllegalStateException.class)
    public void acquireAfterTransactionStarted() {
        // given
        DbWriteManager.setTransactionDetector(() -> true);

        // when
        DbWriteManager.acquire();

        // then
        // exception is thrown
    }

    @Test
    public void acquireWorks() {
        // given
        DbWriteManager.setTransactionDetector(() -> false);

        // when
        DbWriteManager.acquire();

        // then
        assertTrue(DbWriteManager.isAcquired());
    }

    @Test
    public void releaseWorks() {
        // given
        DbWriteManager.setTransactionDetector(() -> false);

        // when
        DbWriteManager.acquire();
        DbWriteManager.release();

        // then
        assertFalse(DbWriteManager.isAcquired());
    }

    @Test
    public void multipleAcquire() {
        // given
        DbWriteManager.setTransactionDetector(() -> false);

        // when
        DbWriteManager.acquire();
        DbWriteManager.setTransactionDetector(() -> true);
        DbWriteManager.acquire();
        DbWriteManager.acquire();
        DbWriteManager.acquire();

        // then
        assertTrue(DbWriteManager.isAcquired());
    }

    @Test
    public void multipleAcquireAndRelease() {
        // given
        DbWriteManager.setTransactionDetector(() -> false);

        // when
        DbWriteManager.acquire();
        DbWriteManager.setTransactionDetector(() -> true);
        DbWriteManager.acquire();
        DbWriteManager.acquire();
        DbWriteManager.acquire();

        DbWriteManager.release();
        DbWriteManager.release();

        // then
        assertFalse(DbWriteManager.isAcquired());
    }
}
