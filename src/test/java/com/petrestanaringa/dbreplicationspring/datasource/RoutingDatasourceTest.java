package com.petrestanaringa.dbreplicationspring.datasource;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RoutingDatasourceTest {

    @Mock
    private DataSource masterDatasource;

    @Mock
    private DataSource standByDatasource;

    @Mock
    private Connection masterConnection;

    @Mock
    private Connection standByConnection;

    private String user = "user1";
    private String password = "pass12fvv";

    private RoutingDatasource routingDatasource;

    @Before
    public void setUp() throws SQLException {
        DbWriteManager.release();

        when(masterDatasource.getConnection()).thenReturn(masterConnection);
        when(standByDatasource.getConnection()).thenReturn(standByConnection);

        when(masterDatasource.getConnection(user, password)).thenReturn(masterConnection);
        when(standByDatasource.getConnection(user, password)).thenReturn(standByConnection);

        routingDatasource = new RoutingDatasource(masterDatasource, standByDatasource);
    }

    @Test
    public void shouldGetConnectionStandBy() throws SQLException {
        // given
        DbWriteManager.release();
        routingDatasource.startDelegateToStandby();

        // when
        Connection connection = routingDatasource.getConnection();

        // then
        assertEquals(standByConnection, connection);
    }

    @Test
    public void shouldGetConnectionStandByWithCredentials() throws SQLException {
        // given
        DbWriteManager.release();
        routingDatasource.startDelegateToStandby();

        // when
        Connection connection = routingDatasource.getConnection(user, password);

        // then
        assertEquals(standByConnection, connection);
    }

    @Test
    public void shouldGetConnectionMaster() throws SQLException {
        // given
        DbWriteManager.release();
        DbWriteManager.setTransactionDetector(() -> false);
        DbWriteManager.acquire();
        routingDatasource.startDelegateToStandby();

        // when
        Connection connection = routingDatasource.getConnection();

        // then
        assertEquals(masterConnection, connection);
    }

    @Test
    public void shouldGetConnectionMasterWithCredentials() throws SQLException {
        // given
        DbWriteManager.release();
        DbWriteManager.setTransactionDetector(() -> false);
        DbWriteManager.acquire();
        routingDatasource.startDelegateToStandby();

        // when
        Connection connection = routingDatasource.getConnection(user, password);

        // then
        assertEquals(masterConnection, connection);
    }

    @Test
    public void shouldGetConnectionMasterWithNoLockWhenDelegateToStandByIsFalse() throws SQLException {
        // given
        DbWriteManager.release();
        DbWriteManager.setTransactionDetector(() -> false);

        // when
        Connection connection = routingDatasource.getConnection();

        // then
        assertEquals(masterConnection, connection);
    }
}
