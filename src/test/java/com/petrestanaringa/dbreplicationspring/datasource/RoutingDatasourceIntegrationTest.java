package com.petrestanaringa.dbreplicationspring.datasource;

import com.petrestanaringa.dbreplicationspring.entities.Book;
import com.petrestanaringa.dbreplicationspring.services.BooksService;
import java.sql.SQLException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoutingDatasourceIntegrationTest {

    @Autowired
    private BooksService bookService;

    @Autowired
    private DbHolder masterDatasource;

    @Autowired
    private DbHolder standbyDatasource;

    @Test
    public void shouldGetMasterForTransactional() throws SQLException {
        masterDatasource.getDataSource().getConnection().createStatement().execute("truncate table books");

        bookService.addNewBook(new Book("Harry Potter", "JK Rowling"));

        boolean hasData = masterDatasource.getDataSource().getConnection().createStatement()
            .executeQuery("select * from books").next();
        Assert.assertTrue(hasData);
    }

    @Test
    public void shouldGetStandByForNonTransactional() throws SQLException {
        standbyDatasource.getDataSource().getConnection().createStatement().execute("DROP ALL OBJECTS");

        bookService.addNewBook(new Book("Harry Potter", "JK Rowling"));

        try {
            bookService.getAll();
            Assert.fail("should throw ex");
        } catch (Exception e) {
            Assert.assertTrue(e.getCause().getCause().getMessage().contains("Table \"BOOKS\" not found;"));
        }
    }
}
